var express = require("express");
var app     = express();
var path    = require("path");
const PORT 	= process.env.PORT || 5000


const { Client } = require('pg');
const client = new Client({
  connectionString: 'postgres://idrpfcjjsolexm:15eaa4858da5c6ae8f470c2b72a3141f3ced7f7486a4f6e8aeabbec26cf6b051@ec2-54-83-15-95.compute-1.amazonaws.com:5432/dabgelhk7u3m43',
  ssl: true,
});

client.connect();

var bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next()
})

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/public/index.html'));
});

app.get('/save',function(req,res){
	
	var query = 'INSERT INTO pushnotification(template, countries) VALUES($1, $2)';
	var values = [req.query.template, req.query.countries];

	client.query(query,values)
				  .then(result => console.log(result))
				  .catch(e => console.error(e.stack))
	res.end();
});

app.use(express.static('public'));

app.listen(PORT);

console.log("Running at Port 5000");