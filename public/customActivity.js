define([
    'postmonger'
], function(
    Postmonger
) {
    'use strict';

    var connection = new Postmonger.Session();
    var payload = {};
    var lastStepEnabled = false;
    var steps = [
        { "label": "Step 1", "key": "step1" },
        { "label": "Step 2", "key": "step2", "active": false }
    ];
    var currentStep = steps[0].key;

    $(window).ready(onRender);

    connection.on('initActivity', initialize);
	connection.on('nextStep', nextStep);
	connection.on('prevStep', prevStep);
	
    function onRender() {
		
        connection.trigger('ready');

        $('#next').click(function() {
            lastStepEnabled = !lastStepEnabled; // toggle status
            steps[1].active = !steps[1].active; // toggle active
			connection.trigger('nextStep');
            connection.trigger('updateSteps', steps);
        });
		
		$('#back').click(function() {
            lastStepEnabled = !lastStepEnabled; // toggle status
            steps[1].active = !steps[1].active; // toggle active
			connection.trigger('prevStep');
            connection.trigger('updateSteps', steps);
        });
    }

    function initialize (data) {

		if (data) {
            payload = data;
        }
    }

    function save() {
        var name = $('#select1').find('option:selected').html();
        var value = getMessage();

        // 'payload' is initialized on 'initActivity' above.
        // Journey Builder sends an initial payload with defaults
        // set by this activity's config.json file.  Any property
        // may be overridden as desired.
        payload.name = name;

        payload['arguments'].execute.inArguments = [{ "message": value }];

        payload['metaData'].isConfigured = true;

        connection.trigger('updateActivity', payload);
    }
	
	function nextStep(){
		var step1 = document.getElementById("divStep1");
		step1.style.display = "none";
		
		var step1 = document.getElementById("divStep2");
		step1.style.display = "block";
	}
	
	function prevStep(){
		var step1 = document.getElementById("divStep1");
		step1.style.display = "block";
		
		var step1 = document.getElementById("divStep2");
		step1.style.display = "none";
		
		var button = document.getElementById("save").disabled=false;
	}
});