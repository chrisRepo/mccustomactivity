function selectCountry(){
	var select = document.getElementById("selCountry");

	var select2 = document.getElementById("selectedCountry");
	var opt = document.createElement('option');
	opt.innerHTML = select.options[select.selectedIndex].innerHTML;
	opt.value = select.options[select.selectedIndex].value;
	select2.appendChild(opt);
	
	select.remove(select.selectedIndex);
}

function unselectCountry(){
	var select = document.getElementById("selectedCountry");

	var select2 = document.getElementById("selCountry");
	var opt = document.createElement('option');
	opt.innerHTML = select.options[select.selectedIndex].innerHTML;
	opt.value = select.options[select.selectedIndex].value;
	select2.appendChild(opt);
	
	select.remove(select.selectedIndex);
}

function setVariable()
{ 
	var select = document.getElementById("selPushVariables");
	var varSelected = select.options[select.selectedIndex].value;
	var textPreview = document.getElementById("pushTextPreview").value			
	var text = document.getElementById("pushText").value;
	
	text = text + varSelected;
	document.getElementById("pushText").value = text;
	
	if(varSelected == "%n%")
		textPreview = textPreview + 'Chris';
	if(varSelected == "%l%")
		textPreview = textPreview + 'Perez';
	if(varSelected == "%d%")
		textPreview = textPreview + '20';
	if(varSelected == "%u%")
		textPreview = textPreview + 'www.adidas.com';
		
	document.getElementById("pushTextPreview").value = textPreview;	
}

function getCountries() {

	$.ajax({
		headers:{  
			"Accept":"application/json",
			"Content-type":"application/json"
		},   url:"https://mock-countries.herokuapp.com/list/",
		success:function(response){
			var sel = document.getElementById('selCountry');
					
			for(var i = 0; i < response.length; i++) {
				var opt = document.createElement('option');
				opt.innerHTML = response[i].name;
				opt.value = response[i].code;
				sel.appendChild(opt);
			}
		}
	});
}

function preview(){
	var src = document.getElementById("pushText").value;
	var text = src.replace(/%n%/g,'Chris');
	text = text.replace(/%l%/g,'Perez');
	text = text.replace(/%d%/g,'20');
	text = text.replace(/%u%/g,'www.adidas.com');
	document.getElementById("pushTextPreview").value = text;
}

function hideButton(){
	var button = document.getElementById("save").disabled=true;
}