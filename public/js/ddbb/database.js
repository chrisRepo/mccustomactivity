
function insertToDDBB(){

	var pushTemplate = document.getElementById("pushText").value;
	var countries = document.getElementById("selectedCountry");
	
	var countriesJSON = [];
	for(var i = 0; i < countries.length; i++) {
		var contryJSON = {'name':countries.options[i].innerHTML,'code':countries.options[i].value};
		countriesJSON.push(contryJSON);
	}
	
	$.ajax({
		url: 'https://dry-brook-62275.herokuapp.com/save?template='+pushTemplate+'&countries='+JSON.stringify(countriesJSON),
		dataType: 'jsonp',
		type: 'get',
		contentType: 'application/json',
		data: JSON.stringify( { "template": pushTemplate, "countries":countriesJSON } ),
		processData: false,
		crossDomain: true,
		headers: {
			'Access-Control-Allow-Origin': '*'
		},
		success:function(response){
			console.log(response);
		}
	});
	
}
